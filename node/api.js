var express = require('express');
var app = express();
var fs = require("fs");
var StellarSdk = require('stellar-sdk');

var stellarServer = new StellarSdk.Server({
    hostname: "horizon-testnet.stellar.org",
    port: 443,
    secure: true
});

var gatewayPath = '../accounts/gateway.txt';
var hotWalletPath = '../accounts/hotwallet.txt';
var gatewayAccount = false;
var hotWalletAccount = false;

var asset = '';
var fundByNewAccount = 10000;
var startingBalance = 200;

var config = {
  configurable: true,
  value: function() {
    var alt = {};
    var storeKey = function(key) {
      alt[key] = this[key];
    };
    Object.getOwnPropertyNames(this).forEach(storeKey, this);
    return alt;
  }
};
Object.defineProperty(Error.prototype, 'toJSON', config);

var init = function() {
    var checkHotWallet = setInterval(function() {
        getHotWalletAccount(function() {
            // console.log('Hot Wallet checked');
        });
    }, '360000');

    var checkGateway = setInterval(function() {
        getGatewayAccount(function() {
            // console.log('Gateway checked');
        });
    }, '360000');

    getGatewayAccount(function(keypair) {
        asset = new StellarSdk.Asset('energy', keypair.public_address);
        console.log('Gateway checked');
        console.log(keypair.public_address);
        console.log(asset);
    });

    getHotWalletAccount(function(keypair) {
        console.log('Hot Wallet checked');
        console.log(keypair.public_address);
    });

}

app.get('/', function(req, res) {
    var request = {};
    request['/keyPair/new'] = { method: 'get', info: 'return a new key pair' };
    request['/keyPair/fromSecret'] = { method: 'get', args: 'secret - the secret key', info: 'return a key pair from secret' };
    request['/account/init'] = { method: 'get', args: 'public - the public address, secret - the secret key', info: 'init an account' };
    request['/account/payment'] = { method: 'get', args: 'public - the public address, secret - the secret key, destination - the public address, quantity - the quantity', info: 'make a payment between two accounts' };
    request['/account/balances'] = { method: 'get', args: 'public - the public address', info: 'return account balances' };
    res.end(JSON.stringify(request));
})

app.get('/keyPair/new', function(req, res) {
    keypair = generateKeyPairs();
    res.end(JSON.stringify(keypair));
})

// app.get('/test', function(req, res) {
//     keypair = StellarSdk.Keypair.fromSeed('SAEHNLSLV3ACLMX3LZE5GQVE4HJDXZ4L7PYPW2Q6FTTA6W3ER2RVJ2LN');
//     res.end(JSON.stringify(keypair));
// })

app.get('/keyPair/fromSecret', function(req, res) {
    try {
        keypair = StellarSdk.Keypair.fromSeed(req.query.secret);
        keypair = keyPairToJson(keypair);
        res.end(JSON.stringify(keypair));

        stellarServer.accounts()
        .accountId(keypair.public_address)
        .call()
        .then(function (result) {
            var energyBalance = false;

            for (i =0; i < result.balances.length; i++) {
                balance = result.balances[i];
                if ((balance.asset_code == asset.getCode()) && (asset.getIssuer() == balance.asset_issuer)) {
                    energyBalance = true;
                }
            }

            var addAssetInAccount = [];
            if (!energyBalance) {
                console.log(result);
                // 1. New accounts accept currency from Gateway
                var createTrustline = StellarSdk.Operation.changeTrust({ asset: asset });
                addAssetInAccount[addAssetInAccount.length] = [createTrustline, keypair.public_address, keypair.secret_key];
                // 1.b Hot Wallet accept currency from Gateway
                addAssetInAccount[addAssetInAccount.length] = [createTrustline, hotWalletAccount.public_address, hotWalletAccount.secret_key];

                // 2. Put assets from Gateway to Hot Wallet
                fundHotWallet = StellarSdk.Operation.payment({ destination: hotWalletAccount.public_address, asset: asset, amount: fundByNewAccount + '' });
                addAssetInAccount[addAssetInAccount.length] = [fundHotWallet, gatewayAccount.public_address, gatewayAccount.secret_key];

                // 3. Hot Wallet put asset in new accounts
                fundNewAccount = StellarSdk.Operation.payment({ destination: keypair.public_address, asset: asset, amount: fundByNewAccount + '' });
                addAssetInAccount[addAssetInAccount.length] = [fundNewAccount, hotWalletAccount.public_address, hotWalletAccount.secret_key];

                console.log('Account without asset detected - ' + JSON.stringify(keypair));

                processTransactionStack(addAssetInAccount, function() {
                    console.log('Account has asset now - ' + JSON.stringify(keypair));
                });
            }

        }).bind(keypair)
        .catch(function (err) {
            fundAccount(keypair);
        }).bind(keypair);

    } catch (e) {
        this.processing = false;
        var alert = {
            title: 'Invalid secret key',
            text: 'Secret keys are uppercase and begin with the letter "S."',
            type: 'INVALID_SECRET_KEY'
        };
        res.end(JSON.stringify({ error: alert }));
    }
})

app.get('/account/balances', function(req, res) {
    try {
        if (!req.query.public) {
            throw 'need a "public" in the request';
        }
        public = req.query.public;
        
        stellarServer.accounts()
        .accountId(public)
        .call()
        .then(function (result) {
            var energyBalance = false;

            var balances = {};
            for (i =0; i < result.balances.length; i++) {
                balance = result.balances[i];
                asset_code = balance.asset_code;
                if (!asset_code) {
                    asset_code = 'native';
                }

                if (asset_code != asset.getCode()) {
                    balances[asset_code] = balance;
                } else if (asset.getIssuer() == balance.asset_issuer) {
                    balances[asset_code] = balance;
                }

            }

            res.end(JSON.stringify({ 
                result: 
                { 
                    msg: 'ok', 
                    status: 'done', 
                    value: balances
                } 
            }));

        })
        .catch(function (err) {
            throw err.name+' - '+err.message.detail;
        });

    } catch (e) {
        var alert = {
            msg: JSON.stringify(e)
        };
        res.end(JSON.stringify({ error: alert }));
    }
})

app.get('/account/init', function(req, res) {
    try {
        if (!req.query.public) {
            throw 'need a "public" in the request';
        }
        $public = req.query.public;
        if (!req.query.secret) {
            throw 'need a "secret" in the request';
        }
        $secret = req.query.secret;

        keyPairTest = StellarSdk.Keypair.fromSeed($secret);
        keyPairTest = keyPairToJson(keyPairTest);

        if (keyPairTest.public_address != $public) {
            throw 'wrong key pair';
        }

        fundAccount(keyPairTest);
        res.end(JSON.stringify({ result: { msg: 'ok', status: 'pending' } }));

    } catch (e) {
        var alert = {
            msg: JSON.stringify(e)
        };
        res.end(JSON.stringify({ error: alert }));
    }
})

app.get('/account/payment', function(req, res) {
    try {
        if (!req.query.public) {
            throw 'need a "public" in the request';
        }
        var public = req.query.public;
        if (!req.query.secret) {
            throw 'need a "secret" in the request';
        }
        var secret = req.query.secret;
        if (!req.query.destination) {
            throw 'need a "destination" in the request';
        }
        var destination = req.query.destination;
        if (!req.query.amount) {
            throw 'need a "amount" in the request';
        }
        var amount = req.query.amount;

        keyPairTest = StellarSdk.Keypair.fromSeed(secret);
        keyPairTest = keyPairToJson(keyPairTest);

        if (keyPairTest.public_address != public) {
            throw 'wrong key pair';
        }

        makePayment(public, secret, destination, amount);

        res.end(JSON.stringify({ result: { msg: 'ok', status: 'pending' } }));

    } catch (e) {
        var alert = {
            msg: JSON.stringify(e)
        };
        res.end(JSON.stringify({ error: alert }));
    }
})

var makePayment = function(public, secret, destination, amount) {
    try {
        console.log('Starting account payment', public, destination, amount);

        // create a new account
        var paymentTransactionStack = [];

        // 1. Transfert form user to destination
        makePaymentTransaction = StellarSdk.Operation.payment({ destination: destination, asset: asset, amount: amount + '' });
        paymentTransactionStack[paymentTransactionStack.length] = [makePaymentTransaction, public, secret];

        processTransactionStack(paymentTransactionStack, function() {
            console.log('Account paid', public, destination, amount);
        });
    } catch (err) {
        console.log('Error while making payment: ' + err);
    }
}

var fundAccount = function(newAccountKeyPair) {
    try {
        console.log('Starting account fund - ' + JSON.stringify(newAccountKeyPair));

        // create a new account
        var newAccountTransactionStack = [];

        // createAccounts by Hot Wallet
        var createAccount = StellarSdk.Operation.createAccount({ destination: newAccountKeyPair.public_address, startingBalance: startingBalance + '' });
        newAccountTransactionStack[newAccountTransactionStack.length] = [createAccount, hotWalletAccount.public_address, hotWalletAccount.secret_key];

        // 2. New accounts accept currency from Gateway
        var createTrustline = StellarSdk.Operation.changeTrust({ asset: asset });
        newAccountTransactionStack[newAccountTransactionStack.length] = [createTrustline, newAccountKeyPair.public_address, newAccountKeyPair.secret_key];
        // 2.b Hot Wallet accept currency from Gateway
        newAccountTransactionStack[newAccountTransactionStack.length] = [createTrustline, hotWalletAccount.public_address, hotWalletAccount.secret_key];

        // 3. Put assets from Gateway to Hot Wallet
        fundHotWallet = StellarSdk.Operation.payment({ destination: hotWalletAccount.public_address, asset: asset, amount: fundByNewAccount + '' });
        newAccountTransactionStack[newAccountTransactionStack.length] = [fundHotWallet, gatewayAccount.public_address, gatewayAccount.secret_key];

        // 4. Hot Wallet put asset in new accounts
        fundNewAccount = StellarSdk.Operation.payment({ destination: newAccountKeyPair.public_address, asset: asset, amount: fundByNewAccount + '' });
        newAccountTransactionStack[newAccountTransactionStack.length] = [fundNewAccount, hotWalletAccount.public_address, hotWalletAccount.secret_key];

        processTransactionStack(newAccountTransactionStack, function() {
            console.log('Account funded - ' + JSON.stringify(newAccountKeyPair));
        });
    } catch (err) {
        console.log('Error while funding account: ' + err);
        setTimeout(function() {
            newAccountKeyPair(newAccountKeyPair)
        }, 5000);
    }
}

var getHotWalletAccount = function(callback) {
    var keypair = readFileSync(hotWalletPath);
    keypair = JSON.parse(keypair);

    var newKeyPair = function() {
        var keypair = generateKeyPairs();
        writeFile(hotWalletPath, JSON.stringify(keypair));
        return keypair;
    }

    if (!keypair) {
        // keypair = newKeyPair();
        keypair = { public_address: '' };
    }

    var publicAddress = keypair.public_address;

    stellarServer.loadAccount(publicAddress)
        .then(function(account) {
            hotWalletAccount = keypair;
            callback(keypair);
        }).bind(publicAddress, keypair)
        .catch(function(err) {
            if (!publicAddress) {
                keypair = newKeyPair();
                hotWalletAccount = keypair;
                publicAddress = keypair.public_address;
            }
            fund(publicAddress, function() {
                console.log('hot wallet funded');
                var transactionStack = [];
                var createTrustline = StellarSdk.Operation.changeTrust({ asset: asset });
                transactionStack[0] = [createTrustline, keypair.public_address, keypair.secret_key];
                processTransactionStack(transactionStack, function() {
                    console.log('hot wallet trustline done');
                    callback(hotWalletAccount);
                });
            });
        }).bind(publicAddress);
}

var getGatewayAccount = function(callback) {
    var keypair = readFileSync(gatewayPath);
    keypair = JSON.parse(keypair);

    var newKeyPair = function() {
        var keypair = generateKeyPairs();
        writeFile(gatewayPath, JSON.stringify(keypair));
        return keypair;
    }

    if (!keypair) {
        // keypair = newKeyPair();
        keypair = { public_address: '' };
    }

    var publicAddress = keypair.public_address;

    stellarServer.loadAccount(publicAddress)
        .then(function(account) {
            gatewayAccount = keypair;
            callback(gatewayAccount);
        }).bind(publicAddress, keypair)
        .catch(function(err) {
            if (!publicAddress) {
                keypair = newKeyPair();
                gatewayAccount = keypair;
                publicAddress = keypair.public_address;
            }
            asset = new StellarSdk.Asset('energy', publicAddress);
            fund(publicAddress, function() {
                callback(gatewayAccount);
            });
        }).bind(publicAddress);
}

var checkAccountBalance = function(publicAddress, success, error) {
    stellarServer.accounts()
        .accountId(publicAddress)
        .call()
        .then(function(result) {
            if (!result.balances) {
                console.log('<strong>Are you sure this account is funded ?</strong>');
                return error('<strong>Are you sure this account is funded ?</strong>');
            }
            // $(result.balances).each(function(key, balance) {
            // 	console.log(balance);
            // });
            success();
        })
        .catch(function(err) {
            error(err);
        });
}

var readFileSync = function(path, callback) {
    var readFileData = '';
    try {
        readFileData = fs.readFileSync(path);
    } catch (err) {
        return false;
    }

    return readFileData;
}

var writeFile = function(path, text, callback) {
    fs.writeFile(path, text, function(err) {
        if (err) {
            return console.log(err);
        }

        console.log('File ' + path + ' written');

        if (callback) {
            callback();
        }
    });
}

var test = function() {
    var keypair = StellarSdk.Keypair.random();
    keypair = keyPairToJson(keypair);
    return keypair; 
}
var generateKeyPairs = function() {
    var keypair = StellarSdk.Keypair.random();
    keypair = keyPairToJson(keypair);
    return keypair;
}

var keyPairToJson = function(stellarSdkKeypair) {
    var keypair = {
        public_address: stellarSdkKeypair.accountId(),
        secret_key: stellarSdkKeypair.seed()
    };
    return keypair;
}

var fund = function(publicAddress, callback, error) {
    stellarServer.friendbot(publicAddress)
        .call()
        .then(function() {
            if (callback) {
                callback();
            }
        })
        .catch(function(err) {
            if (error) {
                error(err);
            }
        });
}

// function to submit operations
var submitOperation = function(operations, accountPublicAddress, accountSecretKey, callback) {

    // Get the source
    stellarServer.loadAccount(accountPublicAddress)
        .then(function(account) {

            // Create a new transaction
            var transaction = new StellarSdk.TransactionBuilder(account);

            if (!Array.isArray(operations)) {
                operations = [operations];
            }
            operations.forEach(function(operation) {
                transaction = transaction.addOperation(operation);
            });
            transaction = transaction.build();
            transaction.sign(StellarSdk.Keypair.fromSeed(accountSecretKey));

            // send the transaction
            stellarServer.submitTransaction(transaction).then(function(result) {
                    if ((typeof debug != 'undefined') && debug) {
                        console.log(result);
                    }
                    if (callback) {
                        callback();
                    }
                })
                .catch(function(err) {
                    console.log(err);
                });
        })
        .catch(function(err) {
            console.log(err);
        });
}

// function to process an operation stack
var processTransactionStack = function(transactionStack, callback) {
    operation = transactionStack.shift();
    if ((typeof debug != 'undefined') && debug) {
        console.log(transactionStack);
    }
    if ((typeof debug != 'undefined') && debug) {
        console.log(operation);
    }

    var processCallback = function() {
        processTransactionStack(transactionStack, callback);
    }

    if (transactionStack.length < 1) {
        processCallback = callback;
    }
    submitOperation(operation[0], operation[1], operation[2], processCallback);
}

init();

var server = app.listen(8081, function() {

    var host = server.address().address
    var port = server.address().port

    console.log("\n\n-=-=-=-=-=-=-=-=-=-= Stellar API launched on http://%s:%s =-=-=-=-=-=-=-=-=-=-\n\n", host, port)

})

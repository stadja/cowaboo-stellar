var debug = false;
var communityAlreadyFound = false;

$('#gettingConfModal').modal({backdrop: 'static', keyboard: false});

var getUrlVars = function() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

var getConfInterval = false;
var getConf = function () {
    $.getJSON('https://gateway.ipfs.io/ipfs/'+ipfsHash, function(conf) {
        clearTimeout(getConfInterval);
        if (!communityAlreadyFound) {
            communityAlreadyFound = true;
            conf.community.forEach( function(member, index) {
                accounts[index] = [member.name, member.address];
            });
            accounts.forEach( function(member, index) {
                $('#checkAccountBalance').append('<option value="'+index+'">'+member[0]+'</option>');
                $('#sender').append('<option value="'+index+'">'+member[0]+'</option>');
                $('#recipient').append('<option value="'+index+'">'+member[0]+'</option>');
                $('#info').append("\n"+member[0]+": "+member[1]);
            });
            $('#gettingConfModal').modal('hide');
        }
    });
};

var accounts = [];

var ipfsHash = getUrlVars()['hash'];
if ((typeof debug != 'undefined') && debug) {
    console.log(ipfsHash);
}

if(ipfsHash) {
    getConfInterval = setInterval(getConf, 2000);
}


var server = new StellarSdk.Server({
    hostname:"horizon-testnet.stellar.org",
    port:443,
    secure: true
});

// function to submit an operation
var submitOperation = function(operations, accountPublicAddress, accountSecretKey, success, error) {

        server.loadAccount(accountPublicAddress)
        .then(function (account) {

            // Create a new transaction
            var transaction = new StellarSdk.TransactionBuilder(account);

            if (!Array.isArray(operations)) {
                operations = [operations];
            }
            operations.forEach(function(operation) {
                transaction = transaction.addOperation(operation);
            });

            try {
                transaction = transaction.build();
                transaction.sign(StellarSdk.Keypair.fromSeed(accountSecretKey));

                // send the transaction
                var promise = server.submitTransaction(transaction);

                if (success) {
                    promise.then(function (result) {
                        success(result);
                    });
                }

                if (error) {
                    promise.catch(function (err) {
                        error(err);
                    });
                }

            } catch(err) {
                if (error) {
                    error({title: 'Error', detail: err});
                }
            }

        });


}

var checkAccountBalanceFunction = function(){
    $('#checkAccountBalanceResult').hide(200);
    $('#checkAccountBalanceError').hide(200);
    var showError = function(title, message) {
        $('.checkAccountBalanceLoader').hide();
        $('#checkAccountBalanceErrorTitle').html(title);
        $('#checkAccountBalanceErrorBody').html(message);
        $('#checkAccountBalanceError').show(200);
    };

    var checkAccountBalance = $('#checkAccountBalance').val();
    if (!checkAccountBalance) {
        return false;
    }

    server.accounts()
    .accountId(accounts[checkAccountBalance][1])
    .call()
    .then(function (result) {
        var energyBalance = false;
        result.balances.forEach(function(balance) {
            if ((balance.asset_code == 'energy') && (balance.asset_issuer == 'GBO5HHNBHNEZBMQY2CA6IKNZCHNTAKYBQAWK375W5U4FZ6Q4JTVHDYBJ')) {
                energyBalance = balance.balance - 10000;
            }

        $('#checkAccountBalanceResultBody').html('This account has a balance of <strong>'+energyBalance+'</strong> energy');
        $('#checkAccountBalanceResult').show(200);
        }); 
    })
    .catch(function (err) {
        showError(err.name, err.message.detail+'<br/><strong>Are you sure this account is funded ?</strong>');
        console.log(err);
        return false;
    });
};
$('#checkAccountBalance').on('change', checkAccountBalanceFunction);
$('#checkAccountBalanceBtn').on('click', checkAccountBalanceFunction);

var transacting = false;
$('#transactionForm').submit(function() {
    if (transacting) {
        return false;
    }
    $('.shownBeforeMakingTransaction').hide();
    $('.shownWhileMakingTransaction').show();
    $('#makeTransactionResult').hide(200);
    $('#makeTransactionError').hide(200);
    var showError = function(title, message) {
        $('#makeTransactionErrorTitle').html(title);
        $('#makeTransactionErrorBody').html(message);
        $('#makeTransactionError').show(200);
        $('.shownBeforeMakingTransaction').show();
        $('.shownWhileMakingTransaction').hide();
        return false;
    };

    var sender = $('#sender').val();
    if (!sender) {
        return showError('No Sender selected !', 'Don\'t forget to select a sender');
    }
    $('#sender').val('').change();

    var recipient = $('#recipient').val();
    if (!recipient) {
        return showError('No recipient selected !', 'Don\'t forget to select a recipient');
    }
    $('#recipient').val('').change();

    var amount = $('#amount').val();
    if (!amount) {
        return showError('No amount entered !', 'Don\'t forget to enter an amount');
    }
    $('#amount').val('');

    var asset = new StellarSdk.Asset('energy', 'GBO5HHNBHNEZBMQY2CA6IKNZCHNTAKYBQAWK375W5U4FZ6Q4JTVHDYBJ');
    var payment = StellarSdk.Operation.payment({destination: accounts[recipient][1], asset: asset, amount: amount});
    
    transacting = true;

    var privatekey = $('#secretKey').val();
    try {
        submitOperation(payment, accounts[sender][1], privatekey, function(result) {
            $('#makeTransactionResultTitle').html('Done !');
            $('#makeTransactionResultBody').html('The transaction has been sent !');
            $('#makeTransactionResult').show(200);
            $('.shownBeforeMakingTransaction').show();
            $('.shownWhileMakingTransaction').hide();
            transacting = false;
            console.log(result);
        }, function(err) {
            showError(err.title, err.detail);
            transacting = false;
            console.log(err);
        });
    } catch(err) {
            console.log(err);
            // showError(err.title, err.detail);
            // transacting = false;
    }

    return false;
});
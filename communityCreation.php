<?php

$mail =  "Hello ".$_POST['member'][0].' !';
$mail .= "<br/><br/>You've been added to a Stellar exchange community.";
$mail .= "<br/>For this, we have created for you a Stellar account:";
$mail .= "<br/>- Public Address: ".$_POST['member'][2];
$mail .= "<br/>- Secret Key: ".$_POST['member'][3];
$mail .= "<br/><strong>WARNING:</strong> DO NOT LOSE your secret key. It is really important, we won\'t be able to send it back to you later and you'll need it to make exchanges.";
$mail .= "<br/><br/>We know that sending this information by email is not a secured way, but it is still early beta. There are many other way for sending you this information (SMS, pidgeons, letters...) but right now it is the easiest way.";
$mail .= "<br/><br/>The last step is to <strong>access your <a href='http://prototype.stadja.net/cowaboo/stellar/community.html?hash=".$_POST['ipfsHash']."'>Stellar exchange community</a></strong>";
$mail .= "<br/><br/>Anyway, we hop you'll enjoy playing with this prototype and start creating exchange of energy in your community.";
$mail .= "<br/><br/>Cheers,";
$mail .= "<br/>The CoWaBoo crew.";

$to = $_POST['member'][1];

$subject = 'Welcome to your Stellar exchange community';

$headers = "From: " . strip_tags('do-no-reply@prototype.stadja.net') . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

return mail($to, $subject, $mail, $headers);

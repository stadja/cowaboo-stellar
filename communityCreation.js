var debug = false;
var server = new StellarSdk.Server({
    hostname:"horizon-testnet.stellar.org",
    port:443,
    secure: true
});
var asset = new StellarSdk.Asset('energy', 'GBO5HHNBHNEZBMQY2CA6IKNZCHNTAKYBQAWK375W5U4FZ6Q4JTVHDYBJ');
var fundByNewAccount = 10000;
var transactionStack = [];
var ipfsHash = false;

var communityInputs = [];

var community = [];
var numberOfTransactions = 0;
var getNumberOfOperations = function() {
    var numberOfOperations = numberOfTransactions + 1 + community.length + 1;
    return numberOfOperations;
}
var getCurrentOperationNumber = function() {
    var currentOperationNumber = (numberOfTransactions - transactionStack.length) + 1;
    if (ipfsHash) {
        currentOperationNumber++;
    }
    var communityTreated = community.filter(function(member) {
        return member[4];
    });
    currentOperationNumber += communityTreated.length;
    return currentOperationNumber;
}

var tick = function(msg) {
    var  total = (100 / getNumberOfOperations()) * getCurrentOperationNumber();
    // var  total = (100 / 3) * 2;
    $('#progressbar').width(total+'%');

    var info = '';
    if (msg) {
        info += '<strong>'+msg+'</strong><br>';
    }
    info += 'Operation '+getCurrentOperationNumber()+'/'+getNumberOfOperations();
    $('#progressbarText').html(info);

    if ((typeof debug != 'undefined') && debug) {
        console.log(info);
    }
}

var finish = function() {
    $('#progressbarModalLabel').html('Community created...');
    var msg = "<strong>It's done !</strong> <br>Check your email to see where to start exchanging with other members of your community ! (if you don't find the mail, check your spams)<br><br> Have fun :)";
    $('#progressbarText').html(msg);
    $('#progressbar').hide();
}

// function to submit operations
var submitOperation = function(operations, accountPublicAddress, accountSecretKey, callback) {

    // Get the source
    server.loadAccount(accountPublicAddress)
    .then(function (account) {

        // Create a new transaction
        var transaction = new StellarSdk.TransactionBuilder(account);

        if (!Array.isArray(operations)) {
            operations = [operations];
        }
        operations.forEach(function(operation) {
            transaction = transaction.addOperation(operation);
        });
        transaction = transaction.build();
        transaction.sign(StellarSdk.Keypair.fromSeed(accountSecretKey));

        // send the transaction
        server.submitTransaction(transaction).then(function (result) {
            if ((typeof debug != 'undefined') && debug) {
                console.log(result);
            }
            if (callback) {
                callback();
            }
        });
    });
}

// function to process operation stack
var processTransactionStack = function() {
    tick('Stellar transactions');
    operation = transactionStack.shift();
    if ((typeof debug != 'undefined') && debug) {
        console.log(transactionStack);
    }
    if ((typeof debug != 'undefined') && debug) {
        console.log(operation);
    }

    var callback = processTransactionStack;
    if (transactionStack.length < 1) {
        callback = processMailStack;
    }
    submitOperation(operation[0], operation[1], operation[2], callback);
} 

var processMailStack = function() {
    
    var notTreatedIndex = -1;
    for (var i = 0; i < community.length; i++) {
        var member = false;

        if ((notTreatedIndex < 0) && !community[i][4]) {
            notTreatedIndex = i;
        }
    }

    if ((typeof debug != 'undefined') && debug) {
        console.log(notTreatedIndex);
    }
    
    if (notTreatedIndex >= 0) {
    tick('Sending a mail to '+community[notTreatedIndex][0]);
    $.ajax('communityCreation.php', {
        method: 'POST',
        data: {
            member:   community[notTreatedIndex],
            ipfsHash: ipfsHash
        },
        success: function(data) {
            community[notTreatedIndex][4] = true;
            processMailStack();
        }
    });
    } else {
        finish();
    }
}
var createACommunity = function() {

    // 1. Create new accounts
    communityInputs.forEach( function(member, index) {
        var keypair = StellarSdk.Keypair.random();
        member[2] = keypair.accountId();
        member[3] = keypair.seed();
        // Is Mail Sent ?
        member[4] = false;
        community[index] = member;
    });

    if ((typeof debug != 'undefined') && debug) {
        console.log(community);
    }
    var amountTotalOfNewAssets =  community.length*fundByNewAccount;
    if ((typeof debug != 'undefined') && debug) {
        console.log(amountTotalOfNewAssets);
    }

    var createAccounts = [];
    community.forEach( function(member, index) {
        createAccounts[index] = StellarSdk.Operation.createAccount({destination: member[2], startingBalance: '200'});
    });
    // createAccounts by Hot Wallet
    transactionStack[transactionStack.length] = [createAccounts, 'GDAGJ5W6NY26BO4RTIVBAGFLBWLBC5OU4FP2DJ4DZLO3ILTHFH3PJPHL', 'SBHCICF5ABUGBSQZYMCIMW3DR6F2WMSGN2763QOGHWXD6SMLY6VNHS6Q'];

    // 2. Put assets from Gateway to Hot Wallet
    fundHotWallet = StellarSdk.Operation.payment({destination: 'GDAGJ5W6NY26BO4RTIVBAGFLBWLBC5OU4FP2DJ4DZLO3ILTHFH3PJPHL', asset: asset, amount: amountTotalOfNewAssets+''});
    // fundHotWallet by Gateway
    transactionStack[transactionStack.length] = [fundHotWallet, 'GBO5HHNBHNEZBMQY2CA6IKNZCHNTAKYBQAWK375W5U4FZ6Q4JTVHDYBJ', 'SCUMRVGMRVX3TUVOWLGQS6V3SWQVQVY65PY6NNTZTDQOAGIWXNA3CTML'];

    // 3. New accounts accept currency from Gateway
    community.forEach( function(member, index) {
        var createTrustline = StellarSdk.Operation.changeTrust({asset: asset});
        // createTrustline by member
        transactionStack[transactionStack.length] = [createTrustline, member[2], member[3]];
    });

    // 4. Hot Wallet put asset in new accounts
    var fundNewAccounts = [];
    community.forEach( function(member, index) {
        fundNewAccounts[index] = StellarSdk.Operation.payment({destination: member[2], asset: asset, amount: fundByNewAccount+''});
    });
    // fundNewAccounts by Hot Wallet
    transactionStack[transactionStack.length] = [fundNewAccounts, 'GDAGJ5W6NY26BO4RTIVBAGFLBWLBC5OU4FP2DJ4DZLO3ILTHFH3PJPHL', 'SBHCICF5ABUGBSQZYMCIMW3DR6F2WMSGN2763QOGHWXD6SMLY6VNHS6Q'];

    numberOfTransactions = transactionStack.length;

    // 5. Save configuration to ipfs
    var communityConfiguration = {};
    communityConfiguration.asset = {
        code: asset.getCode(),
        type: asset.getAssetType(),
        issuer: asset.getIssuer(),
    };

    communityConfiguration.community = community.map(function(member) {
        var memberDescription = {};
        memberDescription.name = member[0];
        memberDescription.email = member[1];
        memberDescription.address = member[2];
        return memberDescription;
    });

    if ((typeof debug != 'undefined') && debug) {
        console.log(communityConfiguration);
    }

    communityConfiguration = JSON.stringify(communityConfiguration);
    if ((typeof debug != 'undefined') && debug) {
        console.log(communityConfiguration);
    }

    var url = 'http://stadja.net:81/rest/cowaboo/ipfs?app_name=cowaboo';
    url += '&group=stellarCommunities';
    url += '&tags='+encodeURIComponent('stellar,community');
    url += '&description='+encodeURIComponent(communityConfiguration);

    if ((typeof debug != 'undefined') && debug) {
        console.log(url);
    }

    tick('Save community definition to IPFS');
    $.ajax({
        url: url,
        method: 'POST',
        success: function(data) {
            if ((typeof debug != 'undefined') && debug) {
                console.log(data);
            }
            ipfsHash = data.Hash;
            processTransactionStack();
        }
    });
}

$('#addAMember').click(function() {
    var newMember = $('#memberExample').clone();
    newMember.attr('id', '');
    newMember.css('display', 'block');
    $('#memberList').append(newMember);
    initSuppress();
});

var initSuppress = function() {
    $('.suppressMember').click(function () {
        this.parentElement.parentElement.remove();
        return false;
    });
}

$('#communityForm').submit(function() {
    var members = [];
    $(this).find('[name="memberName[]"]').each( function(index, memberName) {
        members[index] = [];
        members[index][0] = memberName.value.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});;
    });
    $(this).find('[name="memberEmail[]"]').each( function(index, memberEmail) {
        members[index][1] = memberEmail.value;
    });
    
    communityInputs = members;
    $('#myModal').modal();
    return false;
});

$('#createThatCommunity').click(function() {
    createACommunity();
});
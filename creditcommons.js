var accounts = [["Alice",
     "GBKQCRBZ6KXXVAZNHVOWTE56AS7IRQLSQNOQZFJ6SUHR3B7QJCDKRMO7",
     "SBWDGRHDJK3A37DC3XS3GCUMGOWV2X5LO3MMNAHOU4O6VBGEKPCHBZOL"
],["Bob",
     "GCHJYVCWCDWTHTR3ILBLXGNVMWJVGBV6CKXTP23EZRYILKAQ5UE5I5GP",
     "SA46QSHEVZBAH2W2AZ2GMU76CSX72U3TIHJTD2OTVJFIGKYABWB3XAEF"
],["Charlie",
     "GAOINNDOXIEV4TYI6AJDHANZAUGKYR3SOATUZWGZYPW2NLVH5FL4AILW",
     "SAKPH7RAIGNWMSBBSSYPG27VBCAZ7C3DNPMHV4F27W7Y4ZLQ3GUX5EYG"
],["Dan",
     "GDAJHYVY77UPWDHX4VQJQ37ML4EVS3PKRJQSV7VTD72KWSUHVZ3BDITI",
     "SCFYTIKI7DOIWPQV3CJXQKVXXNZSPHG63UOD4DTEL2ANUXMJIGHO5UG6"
],["Erin",
     "GD5JBVSDA4WMLQUQDXSU5G2X5JGMHBZOP5PJXJAKFS65HTKFBMUZYMR6",
     "SC23DPU54CLYBT5GHEI527AMXAYVSKLPJBPY7WFHKDHFI6ADSEFS6OU5"
],["Franck",
     "GDO4245S6T4AJ3O6Q7KVY6HJA5RO5F7IFNBTVKTORQYFX44ZQOJTEEXN",
     "SCIJ6LRAQY7YNSJPFOAAO62NMPDLBHJUHSVCGJ65HZDPADPWWJNOC66Q"
],["George",
     "GASAD53EY7BOMQBZ62VY2MOHMHHQGC7LHRFYHOENSQSTOPT7QJ3NYV2G",
     "SA5B25FRVHYWPU5BVTSPXOONXZQUSMUCR5QDSJ2OWGZC2YEVTWQI6A4S"
],["Henry",
     "GACCZAXVBOESC4Y5WFBIHFRF3KDR5IEDWVZ72RLMRREAOGAZAOZ3FXTH",
     "SDXWSGXWSFXJR3PGO75K46REEJT5LRIQEWBRG2RYTUXNUPTYB5VLZIST"
],["Ismael",
     "GBZ7SP7JBH3IKJIOJ4BZEBTLBLSOU7D4W6ZNZXHLRHT4TWQRLE6IC5EM",
     "SB6BPTA4HEXXQ3NE2NI2JNASUBT3LZD3LGNVMBPA5GNCFTVMKXIUZIT5"
],["Joe",
     "GDLBQZX3QFQMNW6VFAIIOJC3XZE5WNE7WYDBQGVRS2E3FZINVRYC72VT",
     "SDDINATUULAWDA5W4AF4DPUB6ZGLPCYB3DYB2FBP3WBSCVNXEL6UZHQX"
]];

accounts.forEach( function(member, index) {
    $('#checkAccountBalance').append('<option value="'+index+'">'+member[0]+'</option>');
    $('#sender').append('<option value="'+index+'">'+member[0]+'</option>');
    $('#recipient').append('<option value="'+index+'">'+member[0]+'</option>');
    $('#info').append("\n"+member[0]+": "+member[1]);
});


var server = new StellarSdk.Server({
    hostname:"horizon-testnet.stellar.org",
    port:443,
    secure: true
});

// function to submit an operation
var submitOperation = function(operations, accountPublicAddress, accountSecretKey, success, error) {

    // Get the source
    server.loadAccount(accountPublicAddress)
    .then(function (account) {

        // Create a new transaction
        var transaction = new StellarSdk.TransactionBuilder(account);

        if (!Array.isArray(operations)) {
            operations = [operations];
        }
        operations.forEach(function(operation) {
            transaction = transaction.addOperation(operation);
        });
        transaction = transaction.build();
        transaction.sign(StellarSdk.Keypair.fromSeed(accountSecretKey));

        // send the transaction
        var promise = server.submitTransaction(transaction);

        if (success) {
            promise.then(function (result) {
                success(result);
            });
        }

        if (error) {
            promise.catch(function (err) {
                error(err);
            });
        }

    });
}

var checkAccountBalanceFunction = function(){
    $('#checkAccountBalanceResult').hide(200);
    $('#checkAccountBalanceError').hide(200);
    var showError = function(title, message) {
        $('.checkAccountBalanceLoader').hide();
        $('#checkAccountBalanceErrorTitle').html(title);
        $('#checkAccountBalanceErrorBody').html(message);
        $('#checkAccountBalanceError').show(200);
    };

    var checkAccountBalance = $('#checkAccountBalance').val();
    if (!checkAccountBalance) {
        return false;
    }

    server.accounts()
    .accountId(accounts[checkAccountBalance][1])
    .call()
    .then(function (result) {
        var energyBalance = false;
        result.balances.forEach(function(balance) {
            if ((balance.asset_code == 'energy') && (balance.asset_issuer == 'GBO5HHNBHNEZBMQY2CA6IKNZCHNTAKYBQAWK375W5U4FZ6Q4JTVHDYBJ')) {
                energyBalance = balance.balance - 10000;
            }

        $('#checkAccountBalanceResultBody').html('This account has a balance of <strong>'+energyBalance+'</strong> energy');
        $('#checkAccountBalanceResult').show(200);
        }); 
    })
    .catch(function (err) {
        showError(err.name, err.message.detail+'<br/><strong>Are you sure this account is funded ?</strong>');
        console.log(err);
    });
};
$('#checkAccountBalance').on('change', checkAccountBalanceFunction);
$('#checkAccountBalanceBtn').on('click', checkAccountBalanceFunction);

var transacting = false;
$('#transactionForm').submit(function() {
    if (transacting) {
        return false;
    }
    $('.shownBeforeMakingTransaction').hide();
    $('.shownWhileMakingTransaction').show();
    $('#makeTransactionResult').hide(200);
    $('#makeTransactionError').hide(200);
    var showError = function(title, message) {
        $('#makeTransactionErrorTitle').html(title);
        $('#makeTransactionErrorBody').html(message);
        $('#makeTransactionError').show(200);
        $('.shownBeforeMakingTransaction').show();
        $('.shownWhileMakingTransaction').hide();
        return false;
    };

    var sender = $('#sender').val();
    if (!sender) {
        return showError('No Sender selected !', 'Don\'t forget to select a sender');
    }
    $('#sender').val('').change();

    var recipient = $('#recipient').val();
    if (!recipient) {
        return showError('No recipient selected !', 'Don\'t forget to select a recipient');
    }
    $('#recipient').val('').change();

    var amount = $('#amount').val();
    if (!amount) {
        return showError('No amount entered !', 'Don\'t forget to enter an amount');
    }
    $('#amount').val('');

    var asset = new StellarSdk.Asset('energy', 'GBO5HHNBHNEZBMQY2CA6IKNZCHNTAKYBQAWK375W5U4FZ6Q4JTVHDYBJ');
    var payment = StellarSdk.Operation.payment({destination: accounts[recipient][1], asset: asset, amount: amount});
    
    transacting = true;
    submitOperation(payment, accounts[sender][1], accounts[sender][2], function(result) {
        $('#makeTransactionResultTitle').html('Done !');
        $('#makeTransactionResultBody').html('The transaction has been sent !');
        $('#makeTransactionResult').show(200);
        $('.shownBeforeMakingTransaction').show();
        $('.shownWhileMakingTransaction').hide();
        transacting = false;
        console.log(result);
    }, function(err) {
        showError(err.title, err.detail);
        transacting = false;
        console.log(err);
    });

    return false;
});